import os 
import sys
import re
import time
import linecache
import vulnscans
from clear import clear
from logo import *


def start():
    global hashed
    with open('hash/target.py', 'r') as f:
        hashed = f.read()
    clear()
    menu()
    

def menu():
    logo(1)
    global hashed
    global leng
    leng = len(hashed)
    print("Current Hash: " + BC.F + hashed)
    print(BC.G + "Hash Length: " + BC.F + str(leng))
    print("")
    mitems2("Search Known Hash DB's", "pyCrack by Holistic Coding", "")
    print("")
    print("")
    print(BC.A + '                  HashCat - Coming Soon' + BC.G)

    sys.stdout.write("\033[F")
    sys.stdout.write("\033[F")
    sys.stdout.write("\033[F")
    print(BC.E + "   Enter An Item Number" + BC.G)
    sys.stdout.write("\033[F")
    mi = input(">" + BC.F)
    menuz(mi)
    print(BC.G + "")
    mp = mi[:7]
    mo = mi[8:]
    np = mi[:5]
    no = mi[6:]
    print(leng)
    print(hashed)
    if mi == "1":
        os.system('python3 hashsearch.py -t ' + hashed)
        menu()
    elif mi == "2":
        global htype
        if leng == 32:
            htype = '-m'
            os.system('python3 pyCrack.py -m %s' % (hashed))
            print('python3 pyCrack.py %s %s' % (htype, hashed))
            menu()
        elif leng == 64:
            htype = '-S'
            os.system('python3 pyCrack.py -S %s' % (hashed))
            print('python3 pyCrack.py -S %s' % (hashed))
            menu()
        elif leng == 40:
            htype = '-s'
            os.system ( 'python3 pyCrack.py -s %s' % ( hashed ))
            print("python3 pyCrack.py -s " + hashed)
            menu()
        else:
            clear()
            print(BC.F + "Hash Type Unknown" + BC.G)
            menu()
    elif mi == "3":
        exit
    elif mi == "*":
        clear()
        quit
    elif mi == "0":
        os.system('pkill slacker.py')
    elif mp.lower() == "#target":
        hashed = mo
        clear()
        menu()
    else:
        clear()
        menu()
        
start()