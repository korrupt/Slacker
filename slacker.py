#!/usr/bin/python3

import os 
from clear import clear
import sys
from logo import *
import time
import vulnscans
import dos
import crawl
import custom
import re
from subprocess import call
import linecache
import metasploit

        
### TODO: 
######### Tools To Add: HashCat, Add Redhawk
######### Custom Tool Menu
######### Add Comments
######### Add SE Tool Kit


### Main Menu(s) ###
def Menus(a):
    if a == 1:
        logo()
        print("What type of trouble are we looking for?")
        mitems = ("Vulnerability Scans", "Hash Cracking", "DoS", "SQLMap", "Metasploit", "Crawlers", "Encrypt/Decrypt Private Messages", "Custom Tools")
        for idx, i in enumerate(mitems, start=1):
            print( BC.G + " [" + BC.F + str(idx) + BC.G + "] " + i)
        else:
            print("------------------------------------------")
            print(" [" + BC.F + "**" + BC.G + "] Help")
            print(" [" + BC.F + "0" + BC.G + "] Exit")
            print("")
            print(BC.E + "   Enter An Item Number" + BC.G)
            sys.stdout.write("\033[F")
            mi = input(">" + BC.F)
            menuz(mi)
            print(BC.G + "")
        mp = mi[:7]
        mo = mi[8:]
        np = mi[:5]
        no = mi[6:]
        if mi == "1":
            Menus(2)
        elif mi == "2":
            Menus(3)
        elif mi == "3":
            Menus(4)
        elif mi == "4":
            Menus(5)
        elif mi == "5":
            Menus(6)
        elif mi == "6":
            Menus(7)
        elif mi == "7":
            Menus(8)
        elif mi == "8":
            Menus(9)
        elif mi == "9":
            Menus(10)
        elif mi == "0":
            quit

        else:
            clear()
            Menus(1)
                
    elif a == 2:
        clear()
        vulnscans.menu()
        Menus(1)
    elif a == 3:
        clear()
        os.system('python3 ./hashc.py')
        Menus(1)
    elif a == 4:
        clear()
        dos.start()
        Menus(1)
    elif a == 5:
        clear()
        os.system('python3 ./sqlmap.py')
        Menus(1)
    elif a == 6:
        clear()
        metasploit.start()
        Menus(1)
    elif a == 7:
        clear()
        crawl.start()
        Menus(1)
    elif a == 8:
        clear()
        os.system('python3 ./rsa.py')
        Menus(1)
    elif a == 9:
        clear()
        custom.start()
        Menus(1)
    else:
        clear()
        menu()
        


Menus(1)