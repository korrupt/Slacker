import os 
import sys
import re
import time
import linecache
import globalt
import torshammer
import hulk
import vulnscans
from clear import clear
from logo import *

def start(): 
    global target
    target = globalt.target
    clear()
    menu()
    

def menu():
    logo()
    global target
    mitems2("TorsHammer", "The Hulk")
    print(BC.E + "   Enter An Item Number" + BC.G)
    sys.stdout.write("\033[F")
    mi = input(">" + BC.F)
    print(BC.G + "")
    menuz(mi)
    mp = mi[:7]
    mo = mi[8:]
    np = mi[:5]
    no = mi[6:]
    if mi == "1":
        torshammer.start()
    elif mi == "2":
        hulk.start()
    elif mi == "*":
        clear()
        quit
    elif mi == "0":
        sys.exit()
    elif mp.lower() == "#target":
        clear()
        start()
    else:
        clear()
        menu()