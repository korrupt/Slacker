import os 
import sys
import re
import time
import linecache
import globalt
import dos
from clear import clear
from logo import *

def start(): 
    with open('TorsHammer/threads.py') as f:
        global threads
        threads = f.read()
    with open('TorsHammer/customArgs.py') as f:
        global customArgs
        global tor
        tor = ""
        global port
        port = ""
        customArgs = f.read()
        
    global target
    target = globalt.target
    clear()
    menu()
    

def menu():
    logo()
    global threads
    global target
    global port
    global tor
    global customArgs
    print("Current Target: [" + BC.F + target + BC.G + "]")
    mitems("Run")
    print(BC.E + "   Enter An Item Number" + BC.G)
    sys.stdout.write("\033[F")
    mi = input(">" + BC.F)
    menuz(mi)
    print(BC.G + "")
    mp = mi[:7]
    mo = mi[8:]
    np = mi[:5]
    no = mi[6:]
    ba = mi[:8]
    bb = mi[9:]
    aa = mi[4:]
    aa = mi[:5]
    if mi == "1":
        clear()
        os.system('python hulk/hulk.py %s' % (target))
        menu()
        #clear()
    elif mi == "!":
        clear()
        dos.menu()
    elif mi == "*":
        clear()
        quit
    elif mi == "0":
        sys.exit()
    elif mp.lower() == "#target":
        clear()
        start()
    elif np == "#help":
        os.system( 'python3 hulk/hulk.py --help' )
        input("Press Enter To Continue...")
        menu()
    else:
        clear()
        menu()