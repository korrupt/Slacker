import globalt
import sys
import os
from clear import *
import subprocess

class BC:
    G = '\033[92m'
    A = '\033[0;37m'
    F = '\033[91m'
    B = '\033[1m'
    E = '\033[0m'


def logo(*gl):
            print(BC.G + "Slacker by")
            print(BC.B + BC.F + " .     .  _____         _____ _______ _______ _____  _____")
            print(" |_____| |     | |        |   |______    |      |   |     ") 
            print(" |     | |_____| |_____ __|__ ______|    |    __|__ |_____") 
            print("                                                          ") 
            print("    _____   _____  ______  _____ __   _  ______             ") 
            print("   |       |     | |     \   |   | \  | |  ____             ") 
            print("   |_____  |_____| |_____/ __|__ |  \_| |_____|             ") 
            print("                                                            ") 
            print(BC.E + BC.G + "         Korrupt" + BC.G)
            try:
                if gl[0] == 1:
                        print("")
            except IndexError:
                print("Global Target: [" + BC.F + globalt.target + BC.G + "]")
                print("")
            
            
    
def menuz(aab):
    md = aab
    mp = md[:7]
    mo = md[8:]
    np = md[:5]
    no = md[6:]

    if mp.lower() == "!target":
        with open('globalt.py', 'w') as f:
            f.write('target = "' + mo + '"')
        globalt.target = mo
        target = mo
    elif np.lower() == "!hash":
        with open('hash/target.py', 'w') as f:
            f.write(no)
        hashed = no
        clear()
        menu()
    elif np.lower() == "+hash":
        with open('hash/target.py', 'w') as f:
            f.write(no)
        hashed = no
        clear()
        os.system('python3 ./hashc.py')
    elif np.lower() == "!sqlt":
        with open('sql/target.py', 'w') as f:
            f.write(no)
        sql = no
        clear()
    elif np.lower() == "+sqlt":
        with open('sql/target.py', 'w') as f:
            f.write(no)
        sql = no
        #clear()
        os.system('python3 ./sqlmap.py')
    elif np.lower() == "!help" or md == "**":
        clear()
        helpm()
        input("Press Enter To Continue...")
        
        
        
        
    
    
    
def mitems2(*a):
    global menuz
    mitems = a 
    for idx, i in enumerate(mitems, start=1):
        print( BC.G + " [" + BC.F + str(idx) + BC.G + "] " + i)
    else:
        print("------------------------------------------")
        print(" [" + BC.F + "**" + BC.G + "] Help Menu")
        print("  [" + BC.F + "*" + BC.G + "] Main Menu")
        print("  [" + BC.F + "0" + BC.G + "] Exit")    
    
        
def mitems(*a):
    global menuz
    mitems = a 
    for idx, i in enumerate(mitems, start=1):
        print( BC.G + " [" + BC.F + str(idx) + BC.G + "] " + i)
    else:
        print("------------------------------------------")
        print(" [" + BC.F + "!" + BC.G + "] Back")
        print(" [" + BC.F + "**" + BC.G + "] Help Menu")
        print(" [" + BC.F + "*" + BC.G + "] Main Menu")
        print(" [" + BC.F + "0" + BC.G + "] Exit")
        
        
def helpm():
    print(BC.G + "How To Use Slacker: ")
    print(BC.F + "         Arguments: " + BC.G)
    print(" [" + BC.F + "!target" + BC.G + "] Set A Global Target [* Used By Default]")
    print("                       IE: !target yourdomain.com")
    print(" [" + BC.F + "!sqlt" + BC.G + "] Set A Global SQLMap Target [* To Be Used By SQLMap By Default]")
    print("                       IE: !sqlt you.com/setme.php?please=1")
    print(" [" + BC.F + "+sqlt" + BC.G + "] Set A Global SQLMap Target And Launch SQLMap Page")
    print("                       IE: +sqlt you.com/setme.php?please=1")
    print(" [" + BC.F + "!hash" + BC.G + "] Set A Global Hash To Crack/Search [* To Be Used By Hash Crackers/Search By Default]")
    print("                       IE: !hash hash_here")
    print(" [" + BC.F + "+hash" + BC.G + "] Set A Global Hash To Crack/Search And Launch Hash Cracking Page")
    print("                       IE: +hash hash_here")
    print(" [" + BC.F + "#target" + BC.G + "] Set A Single-Tool Use Target")
    print("                       IE: #target yourdomain.com")
    print(" [" + BC.F + "#port" + BC.G + "] Set A Global Target")
    print("                       IE: #port 22")
    print(" [" + BC.F + "#args" + BC.G + "] Set Custom Arguments")
    print("                       IE: #args -Pn -Sv")
    print(" [" + BC.F + "#help" + BC.G + "] See What Custom Arguments Are Available For That Tool.")
    print("                       IE: #help")
    print(" [" + BC.F + "!help" + BC.G + "] Show This Menu.")
    print("                       IE: !help")
    print("NOTES:")
    print("All Global Arguments Are Set Using ! - All Local Variables Are Set With #")
    print("Tools That Can't Use Local Variables, Such As SQLMap, Will Not Use Global Variables.")
    print("")


