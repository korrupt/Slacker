import os 
import sys
import re
import time
import linecache
import globalt
import vulnscans
from clear import clear
from logo import *

def start(): 
    global target
    target = globalt.target
    clear()
    menu()
    

def menu():
    logo()
    global target
    mitems2("Admin Page Finder (AAP)", "Subdomainer")
    print(BC.E + "   Enter An Item Number" + BC.G)
    sys.stdout.write("\033[F")
    mi = input(">" + BC.F)
    print(BC.G + "")
    menuz(mi)
    mp = mi[:7]
    mo = mi[8:]
    np = mi[:5]
    no = mi[6:]
    if mi == "1":
        clear()
        os.system('python3 aapfinder/aapfinder.py  -u ' + target)
        menu()
    elif mi == "2":
        clear()
        os.system('sudo python3 SubDomainizer/SubDomainizer.py  -u ' + target)
        menu()
    elif mi == "3":
        exit
    elif mi == "*":
        clear()
        quit
    elif mi == "0":
        sys.exit()
    elif mp.lower() == "#target":
        clear()
        start()
    elif np.lower() == "!help" or mi == "**":
        clear()
        helpm()
        input("Press Enter To Continue...")
        menu()
    else:
        clear()
        menu()