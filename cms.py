import os 
import sys
import re
import time
import linecache
import globalt
import vulnscans
from clear import clear
from logo import *

def start(): 
    global target
    target = globalt.target
    clear()
    menu()
    

def menu():
    logo()
    global target
    print("Current Target: " + target)
    mitems("JoomScan", "WPScan", "DroopeScan")
    print(BC.E + "   Enter An Item Number" + BC.G)
    sys.stdout.write("\033[F")
    mi = input(">" + BC.F)
    print(BC.G + "")
    menuz(mi)
    mp = mi[:7]
    mo = mi[8:]
    np = mi[:5]
    no = mi[6:]
    if mi == "1":
        os.system("perl joomscan/joomscan.pl -u " + target)
        menu()
    elif mi == "2":
        os.system("wpscan --ignore-main-redirect --url " + target)
        menu()
    elif mi == "3":
        os.system("droopescan scan drupal --url " + target)
        menu()
    elif mi == "!":
        clear()
        vulnscans.menu()
    elif mi == "*":
        clear()
        quit
    elif mi == "0":
        sys.exit()
    elif mp.lower() == "#target":
        clear()
        start()
    else:
        clear()
        menu()